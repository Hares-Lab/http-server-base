import re
import socket

from example_1.example_1_handlers import Arithmetic_RequestHandler, Dynamic_RequestHandler
from http_server_base import ApplicationBase

class ExampleServer(ApplicationBase):
    handlers = ApplicationBase.default_handlers + \
    [
        (r'^/calc$', Arithmetic_RequestHandler),
    ]
    
    def add_dynamic_handlers(self):
        names = \
        [
            'localhost',
            socket.getfqdn(),
        ]
        source_names = \
            [ (name, name, 'hostname') for name in names ] + \
            [ (socket.getfqdn(name), name, 'full FQDN') for name in names ] + \
            [ (socket.gethostbyname(name), name, 'IP-address') for name in names ]
        
        for address, source_name, source_type in source_names:
            self.logger.info(f"Adding handler for '{address}' ({source_type} for the {source_name}")
            self.add_handlers \
            (
                host_pattern=re.escape(address.lower()),
                host_handlers=[(r'^/dynamic', Dynamic_RequestHandler, dict(address=address, source_name=source_name, source_type=source_type))]
            )

@ExampleServer.simple_start_decorator_with_settings(static_files='static')
def start(server_application: ExampleServer):
    server_application.add_dynamic_handlers()
    
if (__name__ == '__main__'):
    start()
