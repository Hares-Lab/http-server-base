import os
import os.path
import urllib.parse

from markdown import markdown
from tornado.httpserver import HTTPRequest

from http_server_base import Logged_RequestHandler

# import markdown.extensions.codehilite, markdown.extensions.extra


class ExplorerHandler(Logged_RequestHandler):
    logger_name = 'explorer-server.handler'
    
    path: str
    def initialize(self, path, **kwargs):
        super().initialize(**kwargs)
        self.path = os.path.abspath(path)
    
    langs = \
    {
        ('.bash', '.sh'): 'bash',
        '.py': 'python',
        '.css': 'css',
    }
    request: HTTPRequest
    def get(self, *args, **kwargs):
        _path: str = self.path + '/' + self.delete_repeating_slashes(urllib.parse.unquote(self.request.path[1:]))
        
        if (not os.path.exists(_path)):
            self.logger.debug("No such file or directory: '{}'", _path)
            self.resp_error(404)
            return
        
        title = next(key for key in reversed(_path.replace('\\', '/').split('/')) if key)
        
        if (os.path.isdir(_path)):
            self.logger.debug("Responding the list of directories and files in {}", _path)
            _index = os.listdir(_path)
            directories = sorted([ x for x in _index if os.path.isdir(_path + '/' + x) and not x.startswith('.')])
            files = sorted([ x for x in _index if os.path.isfile(_path + '/' + x) ])
            self.render_dynamic_template(title=title, index=directories + files)
            return
        
        elif (_path.lower().endswith('.md')):
            self.logger.debug("Converting .md to .html")
            self.render_markdown(file_path=_path, title=title)
            return
        
        elif (_path.lower().endswith('.html')):
            _file = open(_path, 'rb')
            _html = _file.read().decode('utf8')
            _file.close()
            self.render_dynamic_template(title=title, html=_html)
        
        elif (_path.lower().endswith(('.png', '.jpg', '.jpeg', '.bmp'))):
            image_path = '/raw' + self.request.path
            self.render_dynamic_template(title=title, image=image_path)
            return
        
        else:
            for _lang_extensions in self.langs:
                if (_path.lower().endswith(_lang_extensions)):
                    _lang = self.langs[_lang_extensions]
                    self.logger.debug("{0} code detected: converting to .html", _lang)
                    self.render_markdown(file_path=_path, title=title, lang=_lang)
                    return
            
            self.logger.debug("Responding file content")
            _file = open(_path, 'rb')
            _content = _file.read().decode('utf8')
            _file.close()
            self.render_dynamic_template(title=title, text=_content)
            return
        
        self.logger.warning("Unsupported operation")
        self.resp_error(400)
    
    def render_markdown(self, file_path, title, lang=None):
        _file = open(file_path, 'rb')
        _content = _file.read().decode('utf8')
        _file.close()
        if (lang):
            _content = f"```{lang}\n{_content}\n```"
        _html = markdown(_content, extensions=[ 'markdown.extensions.codehilite', 'markdown.extensions.extra', ] )
        self.render_dynamic_template(title=title, html=_html)
    
    def render_dynamic_template(self, title, html='', text='', image=None, index=None, **kwargs):
        super().render(template_name='dynamic_html_template.html', title=title, html=html, text=text, image=image, index=index, **kwargs)
    
    @classmethod
    def delete_repeating_slashes(cls, s: str) -> str:
        return '/'.join(part for part in s.split('/') if part)
