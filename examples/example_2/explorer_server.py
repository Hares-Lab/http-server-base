from tornado.web import StaticFileHandler

from example_2.explorer_handler import ExplorerHandler
from http_server_base import ApplicationBase
from http_server_base.tools import ConfigLoader


class ExplorerServer(ApplicationBase):
    logger_name = 'explorer-server'
    
    root_dir: str
    
    def __init__(self, root_dir=None, *args, **kwargs):
        self.root_dir = root_dir or ConfigLoader.get_from_config('rootDir', default='.')
        self.handlers = \
        [
            (r'/raw/(.*)', StaticFileHandler, dict(path=self.root_dir)),
            (r'(.*)', ExplorerHandler, dict(path=self.root_dir)),
        ]
        super().__init__(*args, **kwargs)
    
if (__name__ == '__main__'):
    ExplorerServer.simple_start_server()
