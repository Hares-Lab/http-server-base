from http_server_base import ApplicationBase, JsonBasicResponder
from http_server_base.restapi import Rest_RequestHandler, rest_method

class Arithmetic_RequestHandler(Rest_RequestHandler):
    
    base_path = '/calc'
    
    logger_name = 'rest_server.arithmetic_handler'
    
    @rest_method('/add', method='POST', query_args=[('a', float), ('b', float)], simple_return=True)
    def add(self, *args, a, b, **kwargs):
        return a + b
    @rest_method('/sub', method='POST', query_args=[('a', float), ('b', float)], simple_return=True)
    def sub(self, *args, a, b, **kwargs):
        return a - b
    @rest_method('/mul', method='POST', query_args=[('a', float), ('b', float)], simple_return=True)
    def mul(self, *args, a, b, **kwargs):
        return a * b
    @rest_method('/div', method='POST', query_args=[('a', float), ('b', float)], simple_return=True)
    def div(self, *args, a, b, **kwargs):
        return a / b

class ExampleServer(ApplicationBase):
    handlers = ApplicationBase.default_handlers + \
    [
        (r'^/calc(.*)$', Arithmetic_RequestHandler),
    ]
    
    responder_class = JsonBasicResponder

if (__name__ == '__main__'):
    ExampleServer.simple_start_server()
