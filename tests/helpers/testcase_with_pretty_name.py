from typing import Optional
from unittest import TestCase

class TestCaseWithPrettyTestName(TestCase):
    
    def getDoc(self) -> Optional[str]:
        # noinspection PyUnresolvedReferences
        doc: str = self._testMethodDoc
        if (doc):
            lines = filter(lambda x: x.strip(), doc.splitlines())
            if (lines):
                return next(lines).strip()
        
        return None
    
    def id(self) -> str:
        sup = super().id()
        doc = self.getDoc() or ''
        doc = doc.rstrip('.').replace('.', '/')
        if (doc):
            _sup, _sep, _ = sup.rpartition('.')
            return _sup + _sep + doc
        else:
            return sup
